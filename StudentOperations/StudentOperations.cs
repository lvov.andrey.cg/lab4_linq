﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace StudentOperations
{
    public record Student(string name, int group, string exam, double mark);
    public class StudentOperations
    {
        public static List<String> GetHighestAverageScoreStudents(List<Student> students){
            var namesWithAverageScore = students.GroupBy(
                student => student.name,
                student => student.mark,
                (studentName, marks) => new
                {
                    name = studentName,
                    averageScore = Math.Round(marks.Average(), 2)
                }
            );

            List<String> result = namesWithAverageScore.Where(
                student => Math.Abs(
                    student.averageScore - namesWithAverageScore.Max(st => st.averageScore)
                    ) < 1e-5
            ).Select(
                student => student.name
            ).ToList();

            return result;
        }

        public static Dictionary<String, double> GetAverageScoreInSubjects(List<Student> students){

            Dictionary<String, double> result = students.GroupBy(
                student => student.exam,
                student => student.mark,
                (examName, marks) => new {
                    name = examName,
                    averageScore = Math.Round(marks.Average(), 2)
                }
            ).ToDictionary(
                exam => exam.name,
                exam => exam.averageScore
            );

            return result;
        }

        public static Dictionary<string, int> GetBestGroupInEachSubject(List<Student> students)
        {
            Dictionary<string, int> result = students.GroupBy(
                student => student.exam,
                student => student,
                (examName, examsStudents) => new {
                    name = examName,
                    bestGroup = examsStudents.GroupBy(
                        student => student.group,
                        student => student.mark,
                        (groupId, marks) => new {
                            group = groupId,
                            averageScore = Math.Round(marks.Average(), 2)
                        }
                    ).OrderByDescending(
                        group => group.averageScore
                    ).First().group
                }
            ).ToDictionary(
                exam => exam.name,
                exam => exam.bestGroup
            );

            return result;
        }
    }
}
