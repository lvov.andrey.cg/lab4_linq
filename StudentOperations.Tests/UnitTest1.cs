using System.Reflection;
using System.Collections.Generic;
using System;
using Xunit;

namespace StudentOperations.Tests
{
    public class UnitTest1
    {

        [Fact]
        public void HighestAverageScoreTestMustReturnOneStudent()
        {
            List<Student> students = new List<Student> {
                new Student("a", 1, "ex1", 5),
                new Student("a", 1, "ex2", 2),
                new Student("a", 1, "ex3", 4),
                new Student("b", 2, "ex1", 2),
                new Student("b", 2, "ex3", 5),
                new Student("b", 2, "ex4", 5),
                new Student("c", 3, "ex3", 3),
            };

            List<String> results = StudentOperations.GetHighestAverageScoreStudents(students);

            Assert.Single(results);
            Assert.Contains("b", results);
        }

        [Fact]
        public void HighestAverageScoreTestMustReturnTwoStudents()
        {
            List<Student> students = new List<Student> {
                new Student("a", 1, "ex1", 5),
                new Student("a", 1, "ex2", 6),
                new Student("a", 1, "ex3", 4),
                new Student("b", 2, "ex1", 2),
                new Student("b", 2, "ex3", 5),
                new Student("c", 1, "ex4", 3),
                new Student("d", 1, "ex4", 5)
            };

            List<String> results = StudentOperations.GetHighestAverageScoreStudents(students);

            Assert.Equal(2, results.Count);
            Assert.Contains("a", results);
            Assert.Contains("d", results);
        }

        [Fact]
        public void HighestAverageScoreTestMustReturnEmptyList()
        {
            List<Student> students = new List<Student>();

            List<String> results = StudentOperations.GetHighestAverageScoreStudents(students);

            Assert.Empty(results);
        }

        [Fact]
        public void AverageScoreInSubjects()
        {
            List<Student> students = new List<Student> {
                new Student("a", 1, "ex1", 5),
                new Student("a", 1, "ex2", 6),
                new Student("a", 1, "ex3", 4),
                new Student("b", 2, "ex1", 2),
                new Student("b", 2, "ex3", 5),
                new Student("c", 1, "ex2", 4),
                new Student("d", 1, "ex2", 5)
            };

            // 3.5, 5, 4.5

            Dictionary<String, double> results = StudentOperations.GetAverageScoreInSubjects(students);

            Assert.Equal(3, results.Count);
            Assert.True(Math.Abs(results["ex1"] - 3.5) < 1e-5);
            Assert.True(Math.Abs(results["ex2"] - 5) < 1e-5);
            Assert.True(Math.Abs(results["ex3"] - 4.5) < 1e-5);
        }

        [Fact]
        public void AverageScoreInSubjectsMustBeEmpty()
        {
            List<Student> students = new List<Student>();

            Dictionary<String, double> results = StudentOperations.GetAverageScoreInSubjects(students);

            Assert.Empty(results);
        }

        [Fact]
        public void BestGroupInEachSubject()
        {
            List<Student> students = new List<Student> {
                new Student("a", 1, "ex1", 5),
                new Student("b", 1, "ex1", 3),
                new Student("c", 2, "ex1", 3),
                new Student("d", 2, "ex1", 2),
                new Student("e", 1, "ex2", 2),
                new Student("f", 1, "ex2", 2),
                new Student("e", 2, "ex2", 5),
                new Student("f", 2, "ex2", 4),
            };

            // ex1 - 1, ex2 - 2

            Dictionary<String, int> results = StudentOperations.GetBestGroupInEachSubject(students);

            Assert.Equal(2, results.Count);
            Assert.Equal(1, results["ex1"]);
            Assert.Equal(2, results["ex2"]);
        }

        [Fact]
        public void BestGroupInEachSubjectMustBeEmpty()
        {
            List<Student> students = new List<Student>();

            Dictionary<String, int> results = StudentOperations.GetBestGroupInEachSubject(students);

            Assert.Empty(results);
        }

    }
}
